This Project Contains the code base to Djnago server for multivac.

Run following command in ubuntu to install python and django:

```
sudo apt update
sudo apt install python3.8
python --version
```



Install pip and virtualenv
```
sudo apt install python3-pip
sudo apt install python3-venv
```


Create and activate Virtualname
```
python -m venv my_env
source my_env/bin/activate
```

Install django and other required libraies inside the activated ENV.
```
pip install django
django-admin --version
pip install django-cors-headers

```

After Testing Deactivate the ENV
`source my_env/bin/deactivate`



